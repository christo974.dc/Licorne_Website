# LiWebsite -- Website for the project Licorne

<img src="img/Logos/logoLicorne.jpg" alt="Drawing" width="30%"/>

[Static website][1] to describe the [Licorne Project][2].

----------
## Table of contents 

- [LiWebsite](#liwebsite)
  * [Website for the project Licorne](#website-for-the-project-licorne)
    + [General](#general)
    + [Plugin](#plugin)
    + [Screenshots](#screenshots)
    + [How to](#how-to)
      - [Add slide "Output Example"](#add-slide--output-example-)
      - [Add a slide "Demonstration"](#add-a-slide--demonstration-)
    + [Quality Aspect](#quality-aspect)

-------------
## General 

This website is developped in HTML, CSS, Bootstrap and Javascript. It use the flatfy theme by [Andrea Galanti](http://www.andreagalanti.it/flatfy.php)




## Plugin 

LiWebsite use a lot of beautiful javascript plugin.

>**Plugin:**

> - [classie.js][3]
> - [jquery-1.10.2][4]
> - [modernizr-2.8.3][5]
> - [stickUp][6]
> - [wow][7]



## Screenshots
<img src="img/Screenshots/screenshot_3.png" alt="Drawing" width="45%"/>
<img src="img/Screenshots/screenshot_feature.png" alt="Drawing" width="45%"/>



## How to 

### Add slide "Output Example"

For modified or add a slide in Output Example.

>**Steps**

> 1. Go in index.html
> 2. find `<!--- Ouptput Examples -->` in code
> 3. copy/paste code `New Slide` below in example
> 4. modified path image and `<h3>`slide name



```html

index.html

<!---------------- Output Examples ---------------------------> 

<!--code-->

<!--New slide-->
<div class="item">
    <div class="view overlay hm-blue-slight">
        <a><img src="img/OutputExample/nameImage.png" class="img-responsive" alt="slide3">
            <div class="mask waves-effect waves-light"></div>
        </a>
        <div class="carousel-caption hidden-xs">
            <div class="animated fadeInDown">
                <h3>Name Slide</h3>
            </div>
        </div>
    </div>
</div>

```






### Add a slide "Demonstration"

>**Steps**

> 1. Upload your video on Youtube
> 2. Go in index.html
> 3. find `<!--- Demonstration -->` in code
> 4. copy/paste code `New slide video` below in example
> 5. edit the id by yours and edit caption title


```html

<!--index.html-->
<!---------------- Demonstration ---------------------------> 

<!--code-->

<!--New slide video-->
    <div class="video-container item ">
        <div class="youtube-video" id='yours_id'></div> 
        <div class="carousel-caption">Video 3</div>
    </div>

```



## Quality Aspect

Use [Opquast Desktop][8] and [Audits][10] (include in Google Chrome 60) for test quality.

<img src="img/performances/opquast_test_licorne.png" alt="Drawing" width="45%"/>
<img src="img/performances/audits_licorne.png" alt="Drawing" width="45%"/>


## Server config

Configuration of OSU liWebsite server is available [here][9].


[1]:http://licorne.univ-reunion.fr/ "Licorne"
[2]:http://osur-wikis.univ-reunion.fr/mediawiki/index.php/Licorne "Project"
[3]:https://github.com/desandro/classie "classie"
[4]:https://jquery.com
[5]:https://modernizr.com
[6]:http://lirancohen.github.io/stickUp/
[7]:http://mynameismatthieu.com/WOW/
[8]:https://desktop.opquast.com/fr/ "Opquast"
[9]:doc/osu_server/liwebsite_server.md
[10]:https://developers.google.com/web/updates/2017/05/devtools-release-notes#lighthouse