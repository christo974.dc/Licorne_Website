# How to

## Update website

```bash 
$ cd /home/dev/www/liwebsite/
$ sudo git pull
$ sudo /etc/init.d/apache2 restart
```


## Update licorne 

```bash 
$ cd /home/dev/licorne_app/licorne
$ sudo git pull
$ sudo /etc/init.d/apache2 restart
```


## Update documentation 

```bash 
$ cd /home/dev/licorne_app/licorne
$ sudo git pull
$ cd /home/dev/licorne_app/licorne/doc/api/apidoc/
$ sudo make html
$ sudo /etc/init.d/apache2 restart
```

## Update tar licorne 

```bash 
$ cd /home/dev/licorne_app/licorne
$ sudo git archive develop --prefix='Licorne/' | sudo sh -c 'gzip > /home/dev/licorne_app/application/licorne_v0.2_develop.tar.gz'
$ sudo /etc/init.d/apache2 restart
```


# Useful commands


## Make symbolic link to documentation licorne

```bash 
$ cd /var/www/licorne/liwebsite/
$ sudo ln -s /home/dev/licorne_app/licorne/doc/api/apidoc/_build/html/ documentation
```

## Extract file 

```bash 
$ tar -xvzf licorne_v0.2.tar.gz 
```
