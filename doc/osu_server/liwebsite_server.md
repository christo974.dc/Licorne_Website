<!-- Titre 
<!-- Titre
% Serveur web liwebsite
% Documentation
-->
# Documentation serveur Liwebsite

## Introduction 

Ceci est la documentation du serveur web qui comprends :

- les informations du serveur
- l'installation, la configuration et la sécurisation d'Apache2
- la maintenance du serveur


## Serveur web 
 
>**info**

> - Adresse IP du serveur : **10.82.61.61**
> - Adresse public : **licorne.univ-reunion.fr**
> - Adresse documentation : **10.82.61.61/documentation**
> - Adresse pour le téléchargement du paquet licorne : **10.82.61.61/download**

### Outils installés

- Apache2
- git
- les outils de développement pour le paquet licorne (build-essential, python-dev, sphinx, ... )


## Configuration de apache

### Dossier Licorne

Le dossier où se trouve le code source du site internet Licorne se trouve 

### Installation Apache

```bash
sudo apt-get install apache2
```

### Configuration 

La seule configuration faite était pour configurer les virtualhosts.

### Virtualhosts

Permettent à Apache de gérer plusieurs arborescences Web. Dans notre cas, il n'y en a qu'un seul, le site web de Licorne.


#### Fichier de configuration licorne

```conf
# /etc/apache2/sites-available/licorne.conf

<VirtualHost 10.82.61.61:80>
 # ServerAdmin contact@grafikart.fr

 # Domaines gérés par ce virtualhost
    ServerName licorne.univ-reunion.fr
    # ServerAlias *.monsupersite.fr

 # Racine Web
    DocumentRoot /var/www/licorne/liwebsite

 # Règles spécifiques s'appliquant à ce dossier
    
    <Directory /var/www/licorne/liwebsite/>
        Options -Indexes +FollowSymLinks

        AllowOverride All
        Require all granted

    </Directory>

    # Où placer les logs pour cette hôte
    #ErrorLog /home/dev/logs/error.log
    #CustomLog /home/dev/logs/access.log combined
</VirtualHost>
```

#### Lier dossier 

Par défaut Apache consulte les sites depuis le dossier ` /var/www `. Comme l'option FollowSymLinks est active, un lien symbolique est fait entre le dossier de notre projet web et le dossier ` /var/www `.

>**info**

> - Chemin d'accès code site web : /home/dev/www/liwebsite
> - lien symbolique vers le répertoire apache : /var/www/licorne/liwebsite


#### Placer la configuration dans le dossier sites-enabled

```bash
sudo a2ensite monsupersite
```
 et relancer Apache.


### Sécurité

Liwebsite est un site web statique (pas de formulaire, pas de PHP, ...), il n'y a pas pour le moment de sécurité majeur mis en place. Seul les droits attribuer aux fichiers disposent d'une attention particulière.

### Mises à jour 

Pour le moment, les mises à jour ce font manuellement.
Les commandes sont disponibles dans le fichier [how_to.md][1].




## Lien utile

**Sécurité**  
1. Le cahier de l'administrateur Debian -- Mise a jour automatique :  
[https://debian-handbook.info/browse/fr-FR/stable/sect.automatic-upgrades.html](https://debian-handbook.info/browse/fr-FR/stable/sect.automatic-upgrades.html)


**Tuto**  
1. Tutoriel suivi pour la mise en place du site :  
[https://www.grafikart.fr/formations/serveur-linux/apache](https://www.grafikart.fr/formations/serveur-linux/apache)  
2. Tutoriel pour la mise en place d'un serveur web complet (PHP, Mysql, sécurité ... )  
[https://www.mistra.fr/tutoriel-linux-serveur-web-apache2.html](https://www.mistra.fr/tutoriel-linux-serveur-web-apache2.html)




[1]:how_to.md