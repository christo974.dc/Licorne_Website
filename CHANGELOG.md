v 1.0.1
    - index.html : href modification for link sphinx licorne documentation 
    on OSU server
    - add how_to.md : command for update files on OSU server 

v 1.0.0

    - add documentation
        - README.md
        - liwebsite_server.md : How to configure Apache2 and 
        liwebsite on OSU server
